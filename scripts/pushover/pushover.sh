#!/usr/bin/env bash
set -ue
test -f ~/.pushover.cfg || {
echo >&2 "No '~/.pushover.cfg' conf file"
cat <<EOF > ~/.pushover.cfg
po_user=""
po_token=""
EOF
exit 1
}

source ~/.pushover.cfg
po_url=https://api.pushover.net/1/messages.json
po_title="$1"
po_message="$2"

generate_data () {
cat << EOF
{
  "user": "$po_user",
  "token": "$po_token",
  "title": "$po_title",
  "message": "$po_message"
}
EOF
}
curl  -H "Accept: application/json" -H "Content-Type: application/json" \
      -X POST \
      --data "$(generate_data)" \
      "$po_url"
set +ue
