#!/usr/bin/env python3
import coloredlogs
import logging
import argparse
import pathlib2
# import contextlib
# import os
import yaml
import sys
import shutil

psdone=".plexsubs.done"

DEBUG=False

logger = logging.getLogger(__name__)
coloredlogs.install(level='DEBUG',logger=logger,fmt="%(levelname)s %(message)s")

def iter_subs_folders(folder,limit,force):
    c=0
    for sf in folder.glob('**/Subs'):
        if not sf.is_dir():
            logger.debug("[not dir] %s" % sf.parent)
        elif (sf/psdone).exists() and not force:
            logger.debug("[done dir] %s" % sf.parent)
        else:
            c+=1
            logger.info("[dir] %s" % sf.parent)
            yield sf
            if limit and c>=limit:
                logger.debug("[dir limit %d]" % limit)
                break

def iter_media_files(folder):
    ext=["mp4"]
    for e in ext:
        for f in folder.glob('*.'+e):
            if f.is_file():
                yield f

def iter_subtitle_files(folder,pattern):
    for f in folder.glob('**/'+pattern):
        if f.is_file():
            if f.parent == folder:
                key = None
            else:
                key = f.parent.name
            yield key,f

def get_subtitle_files(folder,pattern):
    subtitles = dict()
    sizes = dict()
    for key,srt in iter_subtitle_files(folder,pattern):
        sze = srt.stat().st_size
        if sze > sizes.setdefault(key,0):
            sizes[key] = sze
            subtitles[key] = srt
    dump(dict((key,str(v)) for key,v in subtitles.items()))
    return subtitles

def move_subtitles_files(subtitles,lng,mediafiles,matches,dryrun,move):
    for srtk,srt in subtitles.items():
        if not srtk in matches:
            logger.error("[%s] Unmatched subtitle folder: %s" % (lng,srtk))
            continue
        media = mediafiles[matches[srtk]]
        dest = media.parent / (media.stem + "." + lng + srt.suffix)
        if dest.is_file():
            logger.debug(dest.name)
        elif dryrun:
            logger.info("%s -> %s" % (srt,dest))
        elif move:
            logger.info("MV %s" % dest.name)
            shutil.move(str(srt),str(dest))
        else:
            logger.info("CP %s" % dest.name)
            shutil.copyfile(str(srt),str(dest))


def dump(obj):
    if DEBUG:
        yaml.dump(obj,sys.stderr,default_flow_style=False)

def error(s,*args):
    sys.stderr.write(s % args + '\n')
    sys.exit(1)

def run():
    ap = argparse.ArgumentParser(
            prog = "plexsubs",
            description = "Copies sub files from torrents in places understood by Plex Media Server")
    ap.add_argument("-n","--dryrun",action="store_true",default=False,
            help = "Do not really copy or move files, just print what would be done.")
    ap.add_argument("-d","--dir",type=pathlib2.Path,default=pathlib2.Path.cwd(),
            help = "Working directory (defaults to CWD).")
    ap.add_argument("-l","--limit",type=int,default=0,
            help = "Stop after LIMIT subs directories.")
    ap.add_argument("-m","--move",action="store_true",default=False,
            help = "Move sub files instead of copying them.")
    ap.add_argument("-f","--force",action="store_true",default=False,
            help = "Force going over done folders again.")
    # ap.add_argument("-c","--cleanup",action="store_true",default=False,
            # help = "Remove unused sub files and Subs folders.")
    args = ap.parse_args()

    for subfolder in iter_subs_folders(args.dir,args.limit,args.force):

        mediafiles = dict((f.stem,f) for f in iter_media_files(subfolder.parent))
        logger.debug("found %d media files" % len(mediafiles))
        dump({str(subfolder): dict((k,str(v)) for k,v in mediafiles.items())})

        # Find language-named english subtitle files
        en_subtitles = get_subtitle_files(subfolder,"*_English.srt")
        logger.debug("[en] found %d subtitle files" % len(en_subtitles))

        # Match subtitles to movies
        srtkeys=set(en_subtitles.keys())
        mediakeys=set(mediafiles.keys())
        matches = dict()
        while len(srtkeys) and len(mediakeys):
            srtk = srtkeys.pop()
            if srtk is None:
                matches[srtk] = mediakeys.pop()
            else:
                for mk in mediakeys:
                    if mk.startswith(srtk):
                        break
                else:
                    logger.warning("Unmatched subtitle folder: %s" % srtk)
                    continue
                mediakeys.remove(mk)
                matches[srtk] = mk
        if len(srtkeys):
            logger.warning("Unmatched subtitle folders: %s" % sorted(list(srtkeys)))

        # Find movie-named subtitle files, assume it’s english
        for mk in list(mediakeys):
            media = mediafiles[mk]
            srt = media.parent / "Subs" / (media.stem + ".srt")
            if srt.is_file():
                subtitles[mk] = srt
                matches[mk] = mk
                mediakeys.remove(mk)

        # Manage language-named english subtitle files
        move_subtitles_files(en_subtitles,"en",mediafiles,matches,args.dryrun,args.move)
        
        # Manage other languages
        languages={
                "fr":"*_French.srt"
                }
        for lng,pattern in languages.items():
            subtitles = get_subtitle_files(subfolder,pattern)
            logger.debug("[%s] found %d subtitle files" % (lng,len(subtitles)))
            move_subtitles_files(subtitles,lng,mediafiles,matches,args.dryrun,args.move)

        if not args.dryrun:
            (subfolder / psdone).touch()

if __name__=="__main__":
    run()
