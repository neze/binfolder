#!/usr/bin/env python3
import sys
import socket
import re
import mimetypes
import time
import requests
import argparse
import os

mimetypes.add_type('text/plain','.sub')
mimetypes.add_type('text/plain','.idx')

def msg_format(f):
    def wrapper(msg,*a,**k):
        if len(a): msg %= a
        if len(k): msg %= k
        msg = '%s\n' % msg
        return f(msg)
    return wrapper

@msg_format
def out(m):
    sys.stdout.write(m)
    sys.stdout.flush()

@msg_format
def log(m):
    sys.stderr.write(m)
    sys.stderr.flush()

@msg_format
def err(m):
    sys.stderr.write(m)
    sys.stderr.flush()
    sys.exit(1)

def parse_dest_arg(hostname):
    try:
        address = socket.gethostbyname(hostname)
    except socket.gaierror as e:
        err('%s: %s',hostname,e.strerror)
    try:
        info = socket.gethostbyaddr(address)
    except socket.herror as e:
        # err('%s: %s',address,e.strerror)
        info = ('unknown','unknown',[address])
    if address == hostname:
        dn = '%s' % info[0]
    else:
        dn = '%s (%s)' % (hostname,info[0])
    ip = info[2][0]
    return dn,ip

size_re = re.compile(r'^(\d*(\.\d+)?)([a-z]*)$',re.I)
size_fact = {
        ('','b'): 1,
        ('k','kb','kib'): 1 << 10,
        ('m','mb','mib'): 1 << 20,
        ('g','gb','gib'): 1 << 30,
        ('t','tb','tib'): 1 << 40,
}
def size(x):
    if isinstance(x,str):
        m = size_re.match(x)
        if m is not None:
            val = float(m.group(1))
            fact = m.group(3)
            for k,v in size_fact.items():
                if fact in k:
                    fact = v
                    break
            else:
                raise ValueError('Cannot parse size unit.')
            return Size(val*fact)
        raise ValueError('Cannot parse size. Examples of accepted formats are "10", "42k", "34.3MB", ".32gib".')
    return Size(x)
class Size(int):
    def __str__(self):
        y,x = 0.,float(self)
        for u in ['B','KiB','MiB','GiB','TiB']:
            y,x = x,x/1024.
            if x < 1.: break
        return '%.4g%s' % (y,u)
    def __add__(self,x):
        return type(self)(super().__add__(x))
    __iadd__ = __add__

class Time(int):
    def __str__(self):
        res = ''
        y,x = 0,int(self)
        for u in ['"',"'"]:
            y,x = x % 60, x // 60
            if y: res = '%d%s' % (y,u) + res
        if x: res = '%d°' % x + res
        return res

class Part(object):
    def __init__(self,a,b):
        self.part = int(a)
        self.total = int(b)
        self.len = len(str(self.total))
    def __str__(self):
        return ('%%%dd/%%%dd' % (self.len,self.len)) % (self.part,self.total)

def get_mimetype(filename):
    res = mimetypes.guess_type(filename)[0]
    if res is None:
        err('Could not guess mimetype for "%s"', filename)
    return res

def parse_file_arg(filelist):
    files = []
    count,size = 0,Size(0)

    for b in filelist:
        n = b.name
        s = Size(os.stat(n).st_size)
        t = get_mimetype(n)
        files.append((n,b,s,t))
        count += 1
        size += s

    return files,count,size

def upload(name,stream,mimetype,ip,port,timeout,offset):
    url = 'http://' + ip + ':' + str(port) + '/upload.json'
    try:
        requests.post(url,timeout=1+offset)
    except requests.ConnectionError:
        return err("Couldn't connect to %s",url)
    with stream as media:
        try:
            now = time.time()
            requests.post(url,timeout=timeout+offset,
                    files = {'files[]': (name,stream,mimetype)})
            log('done')
            return time.time() - now
        except requests.ConnectionError:
            return err("Upload error")

def main():
    ap = argparse.ArgumentParser(prog='vlc',
            description='Uploads files to the VLC application on a mobile device.')
    dest_default = { 'default': os.getenv('VLC') }
    if dest_default['default'] is None:
        del dest_default['default']
        dest_default['required'] = True
    ap.add_argument('-d','--dest',type=str,
            help='IP or FQDN address of the mobile device.',
            **dest_default)
    ap.add_argument('-p','--port',type=int,
            help='Destination port if not 80',
            default=80)
    ap.add_argument('-t','--rtt',type=int,
            help='Timeout offset due to RTT.',
            default=0)
    ap.add_argument('-r','--bitrate',type=size,default='512k',
            help='Minimum acceptable bitrate')
    ap.add_argument('file',nargs='+',type=argparse.FileType(mode='rb'),
            help='List of files to be uploaded.')

    args = ap.parse_args()
    name,ip = parse_dest_arg(args.dest)
    port = args.port
    offset = max(0,args.rtt)
    files,count,totalsize = parse_file_arg(args.file)
    bitrate = args.bitrate

    log('Will upload %d file(s) for a total of %s to %s [%s]...',
            count, totalsize, name, ip)

    for i,(name,stream,filesize,mimetype) in enumerate(files):
        log('\n(%s) %s',Part(i+1,count),name)
        log('  minimum  bitrate: %s/s',bitrate)
        timeout = Time(1+filesize/bitrate)
        log('  size: %8s    timeout: %8s    type: %s',filesize,timeout,mimetype)
        spent = upload(name,stream,mimetype,ip,port,timeout,offset)
        out(name)
        bitrate = Size(int(filesize / spent))
        log('  measured bitrate: %s/s',bitrate)
        bitrate = Size(min(max(args.bitrate,int(.7*bitrate)),2*args.bitrate))

if __name__=='__main__':
    try:
        main()
        sys.exit(0)
    except KeyboardInterrupt:
        err('Interrupted')
