#!/usr/bin/env bash
if [ $(id -u) -ne 0 ]; then
  echo >&2 "sudo..."
  sudo $(realpath "$0") "$HOME" "$@"
  exit $?
fi
home="$1"; shift
conf_dir="$home/.local/conf/openvpn"
ovpn="/etc/openvpn"
if ! [ -d "${conf_dir}" ]; then
  echo >&2 "No conf dir: '${conf_dir}'"
  exit 1
fi
conf_dir="${conf_dir}/${1:-client}"
if ! [ -d "${conf_dir}" ]; then
  echo >&2 "No conf dir: '${conf_dir}'"
  exit 1
fi
missing_file=false
for filename in ca.crt client.conf client.key client.crt credentials; do
  if [ ! -f "${conf_dir}/${filename}" ]; then
    echo >&2 "Missing file: '${conf_dir}/${filename}'"
    missing_file=true
  fi
done
if ${missing_file}; then
  exit 1
fi
systemctl stop openvpn@client.service
for filename in ca.crt client.conf client.key client.crt credentials; do
  cp -vf "${conf_dir}/${filename}" "${ovpn}/${filename}"
done
systemctl start openvpn@client.service
