#!/usr/bin/env bash
set -ue
test_country () {
  country="$(curl -Ls ifconfig.co/country-iso)"
  test -n "$country"
  ! grep -q 'FR' <<< "$country"
}

for i in `seq 1 10`; do
  ! test_country || exit 0
  sleep 5
done
exit 1
