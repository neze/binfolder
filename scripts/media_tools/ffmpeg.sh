#!/bin/bash
#ffmpeg -i ${1}                 # input file
#       -vf scale=960:540       # scale keep aspect ratio
#       -vb 1000k               # video bitrate
#       -r 23.976               # frames per second
#       -pix_fmt yuv420p        # picture format
#       -vcodec libx264         # video codec (h264)
#       -ab 256k                # audio bitrate
#       -ar 44100               # audio sampling rate
#       -acodec libvo_aacenc    # audio codec (aac)
#       -y                      # --yes allow to override output
#       ${1%.*}.mp4             # output file
while [ $# -gt 0 ]; do
  file="${1}"; shift;
  ffmpeg \
        -i "${file}"           \
        -threads 0             \
        -vf scale=960:540      \
        -pix_fmt yuv420p       \
        -vcodec libx264        \
        -ar 44100              \
        -ab 256k               \
        -acodec ac3            \
        -y                     \
        "${file%.*}.mp4";
done
