#!/usr/bin/env bash
set -eu
while [ $# -gt 0 ]; do
  mkvsrc="${1}"; shift
  filename="${mkvsrc%.*}"
  trackids=($(mkvinfo "$mkvsrc" |
    sed -n '/^|+ Tracks/,/^|+/ p' |
    grep -B3 'type: subtitles$' |
    grep -o 'track ID.*:\s*[0-9]\+' |
    grep -o '[0-9]\+$' || true))
  #echo "${#trackids[*]}"
  if [ "${#trackids[*]}" -gt 0 ]; then
    if [ "${#trackids[*]}" -gt 1 ]; then
      for id in "${trackids[*]}"; do
        mkvextract tracks "$mkvsrc" $id:"$filename.$id.srt"
      done
    else
      mkvextract tracks "$mkvsrc" ${trackids[0]}:"$filename.srt"
    fi
  fi
done
