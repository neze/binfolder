test ! -f "${2}" || exit 1
HandBrakeCLI --encoder x264 \
             --vb 964 \
             --aencoder ac3 \
             --ab 128 \
             --maxHeight 540 \
             --maxWidth 960 \
             --non-anamorphic \
             --input "${1}" \
             --output "${2}.part" &&
mv "${2}.part" "${2}"
