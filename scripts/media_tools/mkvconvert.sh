#!/usr/bin/env bash
set -ue
ZERO="$(realpath "${BASH_SOURCE[0]-$0}")"
PUSHOVER="$(realpath ~/bin/scripts/pushover/pushover.sh)"

sed="$(which gsed || which sed)"
trash="$(which trash-put || echo "$(which trash) -F")"

show_help () {
cat >&2 << EOF

usage: $(basename "$ZERO") [-h] [-d <dir>] [-s|-S] [-p|-P] <input>

options:
  -h           Show this help.

  -d <dir>     Use <dir> as an output directory.

  -s (default) Get subtitle (srt) tracks.
  -S           Do not get subtitle tracks.

  -p (default) Use a temporary extension (.part).
  -P           Directly output with definitive file name.

  -r           Remove source files when done.
  -R (default) Do not remove source files when done.
EOF
}

error () {
  echo >&2 "error: $@"
  "$PUSHOVER" "MKV Convert ERROR" "error: $@"
  show_help
  exit 1
}

get_subs_list () {
  mkvinfo --ui-language en_US "$1" | ${sed} -n '/+ Track$/ { s/^.*$/-/; p; } ; /+ Track number:/ { s/.*: \([0-9]\+\))/id=\1/; p; }; /+ Track type:/ { s/^.*Track type: /type=/; p; }; /+ Language:/ { s/^.*Language: /lang=/; p; };' | tr '\n' ',' | ${sed} 's/,$//; s/,-,/\n/g; s/-,//g;' | grep 'type=subtitle' | ${sed} 's/id=//;s/type=[^,]*//;s/lang=//;s/,,/,/g'
}

OUTPUT_DIR="$(pwd)"
GET_SRT=true;
USE_PART=true;
RM_SRC=false;

while [ $# -gt 0 ]; do
  arg="$1";
  case "$arg" in
    -h) show_help; exit 2;;
    -d) test -d "$2" || error "Could not find directory '$2'.";
        OUTPUT_DIR="$(realpath "$2")"; shift; shift;;
    -s) GET_SRT=true; shift;;
    -S) GET_SRT=false; shift;;
    -p) USE_PART=true; shift;;
    -P) USE_PART=false; shift;;
    -r) RM_SRC=true; shift;;
    -R) RM_SRC=false; shift;;
    --) shift; break;;
    -*) error "Unknown option '$arg'.";;
    *)  break;;
  esac
done

echo >&2 "output  : '$OUTPUT_DIR'"
echo >&2 "get srt : '$GET_SRT'"
echo >&2 "use part: '$USE_PART'"

T="$(mktemp -d)"
slist="$T/subs_list.txt"
trap "rm -rf '$T'" EXIT

while [ $# -gt 0 ]; do
  mkv="$1"; shift;
  echo >&2 "file    : '$mkv'"

  test -f "$mkv" || error "Could not find file '$mkv'.";
  mkv="$(realpath "$mkv")"
  bn="$(basename "$mkv")"
  fn="${bn%.*}"

  if $GET_SRT; then
    get_subs_list "$mkv" > "$slist"
    count="$(wc -l "$slist" | awk '{ print $1 }')"
    if [ "$count" -gt 0 ]; then
      if [ "$count" -gt 1 ]; then
        cat "$slist" | {
          while IFS=, read id lang; do
            srtname="$OUTPUT_DIR/$fn.${lang:-$id}.srt"
            test -f "$srtname" || mkvextract "$mkv" tracks $id:"$srtname" || true
          done;
        }
      else
        IFS=, read id lang < "$slist"
        test -f "$OUTPUT_DIR/$fn.srt" || mkvextract "$mkv" tracks $id:"$OUTPUT_DIR/$fn.srt" || true
      fi
    else
      :
      #error "Could not find subtitles."
    fi
  fi

  of="$OUTPUT_DIR/$fn.mp4"
  if $USE_PART; then
    pf="$T/$fn.mp4.part"
  else
    pf="$of"
  fi
  if test ! -f "$of"; then
    test ! -f "$of" || error "Output file '$of' already exists."
    test ! -f "$pf" || error "Temporary file '$pf' already exists."
    HandBrakeCLI --encoder x264   \
                --vb 964         \
                --aencoder ac3   \
                --ab 128         \
                --maxHeight 540  \
                --maxWidth 960   \
                --non-anamorphic \
                --input "$mkv"   \
                --output "$pf"   ||
    error "Video conversion did not go well."
    if $USE_PART; then
      mv "$pf" "$of"
    fi
  fi
  "$PUSHOVER" "MKV Convert DONE" "$(basename "$mkv")"
  if $RM_SRC; then
    ${trash} "$mkv"
  fi
done
"$PUSHOVER" "MKV Convert OVER" "OVER"
