#!/usr/bin/env python3
import argparse,logging,sys,os,subprocess,pickle,re
from colorlog import ColoredFormatter

def getLogger():
    return logging.getLogger(sys.argv[0])

def debug(msg,*args,**kwargs):
    getLogger().debug(msg.format(*args,**kwargs))

def info(msg,*args,**kwargs):
    getLogger().info(msg.format(*args,**kwargs))

def warn(msg,*args,**kwargs):
    getLogger().warning(msg.format(*args,**kwargs))

def error(fail=0):
    def __error(msg,*args,**kwargs):
        getLogger().error(msg.format(*args,**kwargs))
        if fail:
            sys.exit(fail)
    return __error

def outerr(s):
    sys.stdout.write(str(s).strip()+'\n')

def critical(msg,*args,**kwargs):
    getLogger().critical(msg.format(*args,**kwargs))
    sys.exit(1)

def logTest():
    debug('debug')
    info('info')
    warn('warn')
    error()('error')
    critical('critical')

def loglevel(debug):
    levels = { 0: logging.ERROR,
               1: logging.WARNING,
               2: logging.INFO,
               3: logging.DEBUG }
    envLevel = os.environ.get('DEBUG',0)
    try:
        envLevel = int(envLevel)
    except Exception:
        envLevel = 0
    argLevel = debug
    level = max(envLevel,argLevel)
    level = levels.get(level,0)
    # logging.basicConfig(level=level)
    LOGFORMAT = "  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
    formatter = ColoredFormatter(LOGFORMAT)
    logging.root.setLevel(level)
    stream = logging.StreamHandler()
    stream.setFormatter(formatter)
    log = getLogger()
    log.setLevel(level)
    log.addHandler(stream)

def parse_args():
    ap = argparse.ArgumentParser(description='series management')
    ap.add_argument('-v',dest='debug',action='count',default=0)
    ap.add_argument('-C',dest='chdir',required=False)
    ap.add_argument('-s',metavar='src-folder',dest='source',default='downloads/done')
    ap.add_argument('-d',metavar='dst-folder',dest='destination',default='videos')
    ap.add_argument('filename',nargs='?',default=None)
    return ap.parse_args()

def debug_args(args):
    attrs = [ attr for attr in dir(args) if not attr.startswith('_') ]
    l = max(map(len,attrs))
    fmt = 'args:{{:{:d}}}:{{:}}'.format(l)
    for attr in attrs:
        debug(fmt,attr,getattr(args,attr))

def getDataFilename():
    return os.path.join(os.path.abspath(os.path.dirname(__file__)),'.data')

_data = None
def getData():
    global _data
    if _data is not None:
        return _data
    filename = getDataFilename()
    if os.path.exists(filename):
        with open(filename,'rb') as input:
            data = pickle.load(input)
    else:
        data = dict()
    if 'series' not in data:
        data['series'] = dict()
    putData(data)
    return data

def putData(data):
    global _data
    filename = getDataFilename()
    with open(filename,'wb') as output:
        pickle.dump(data,output,pickle.HIGHEST_PROTOCOL)
    _data = data
    return

def check_path(path,create=False):
    if not os.path.exists(path):
        if create:
            warn('{:}: creating directory',path)
            os.mkdir(path)
        else:
            error(1)('{:}: no such file or directory',path)

def read_args(args):
    loglevel(args.debug)
    debug_args(args)
    delattr(args,'debug')
    if args.filename is not None:
        func,infos = analyze_filename(args.filename)
        infos['debug']=True
        func(args.filename,'',**infos)
        sys.exit(0)
    if args.chdir is not None:
        chdir = os.path.abspath(args.chdir)
        check_path(chdir)
        os.chdir(chdir)
    for attr in ['source','destination']:
        setattr(args,attr,os.path.abspath(getattr(args,attr)))
    check_path(args.source)
    check_path(args.destination,create=True)
    # xcl = list(map(lambda x: os.path.abspath(x), args.exclude))
    # for f in xcl:
        # check_path(f)
    # xcl.append(args.destination)
    debug_args(args)

def init():
    args = parse_args()
    read_args(args)
    return args

def getout(cmd):
    sp = subprocess.run(cmd,stdout=subprocess.PIPE)
    return sp.stdout.decode('utf-8')

def subfolders(folder):
    cmd = ['find',folder,'-mindepth','1','-maxdepth','1','-type','d']
    debug(' '.join(cmd))
    out = sorted(filter(lambda x: os.path.exists(x), getout(cmd).split('\n')))
    for o in out:
        debug('out:{:}',o)
        yield o

def has_dotpart(folder):
    cmd = ['find',folder,'-name','*.part']
    debug(' '.join(cmd))
    out = getout(cmd)
    res = len(out) > 0
    debug('has_dotpart({:}):{:}',folder,res)
    return res

def done_subfolders(folder):
    for f in subfolders(folder):
        if not has_dotpart(f):
            debug('done folder:{:}',f)
            yield f

def big_files(folder):
    # cmd = ['find',folder,'-type','f','-size','+10M']
    cmd = ['find',folder,'-type','f','(','-size','+10M','-o','-name','*.idx','-o','-name','*.srt','-o','-name','*.sub',')']
    debug(' '.join(cmd))
    out = sorted(filter(lambda x: os.path.exists(x), getout(cmd).split('\n')))
    for o in out:
        debug('big file:{:}',o)
        yield o

def big_done_files(folder):
    cmd = ['find',folder,'-mindepth','1','-maxdepth','1','-type','f','-size','+10M','-not','-name','*.part']
    debug(' '.join(cmd))
    out = filter(lambda x: os.path.exists(x), getout(cmd).split('\n'))
    for o in out:
        debug('big done file:{:}',o)
        yield o

sxp = re.compile(r'[\s.]+')
def normalize_seriesname(seriesname):
    seriesname = sxp.sub(' ',seriesname).lower()
    return seriesname

def known_series(seriesname):
    data = getData()
    if seriesname in data['series']:
        return data['series'][seriesname]
    else:
        try:
            out = input('Write series name for "{:}":\n\t'.format(seriesname))
        except EOFError:
            error(1)('End Of File Error')
        if out:
            data['series'][seriesname] = out
            putData(data)
            return out
        else:
            return None

toremove = re.compile('[^a-zA-Z0-9]')

rxp = list()
rxp.append(re.compile(r'^(.*)[\s.]s([0-9]+)\s*e([0-9]+(-?e[0-9]+)?)[\s.].*$',re.I))
rxp.append(re.compile(r'^(.*)[\s.](1?[0-9])([0-9][0-9])[\s.].*$',re.I))
numrxp = re.compile(r'^.*\.([0-9]+|[a-z]{0,4})\.[^.]+$',re.I)
def analyze_filename(filename):
    filename = os.path.basename(filename)
    match = None
    for rx in rxp:
        match = rx.match(filename)
        if match:
            break
    if not match:
        info('NOT A SERIES: {:}',filename)
    else:
        info('MAYBE SERIES: {:}',filename)
        seriesname = normalize_seriesname(match.group(1))
        season = int(match.group(2))
        episode = list(map(int,match.group(3).replace('E','e').replace('-','').split('e')))
        info('MAYBE SERIES: {:} S{:02d} E{:}',seriesname,season,episode)
        realname = known_series(seriesname)
        if realname:
            info('IS  A SERIES: {:}',realname)
            match = numrxp.match(filename)
            num = match.group(1) if match else 0
            return manage_series, { 'name': realname, 'season': season, 'episode': episode, 'num': num }
        else:
            info('NOT A SERIES: {:}',filename)
    return None,None

def manage_series(f,d,name=None,season=None,episode=None,num=0,debug=False):
    # d = os.path.join(d,'series')
    episode = '-'.join('{:02d}'.format(x) for x in episode)
    info('manage_series {:} {:} {:}s{:}e{:}',f,d,name,season,episode)
    ext = os.path.splitext(f)[1]
    if num: ext = '.' + num + ext
    newname = '{:} - s{:02d}e{:}{:}'.format(name,season,episode,ext)
    if debug:
        info(newname)
        return
    d = os.path.join(d,'{:}'.format(name))
    if not os.path.exists(d):
        cmd = ['mkdir','-p',d]
        out = getout(cmd)
    d = os.path.join(d,newname)
    cmd = ['cp','-n',f,d]
    getout(cmd)
    return d

def manage_file(filename,destination,outfile):
    # info('file: {:}',filename)
    manage,kwargs = analyze_filename(filename)
    if manage is not None:
        dest=manage(filename,destination,**kwargs)
        outfile.write(str(dest).strip()+'\n')

def manage_done_folder(path):
    info('done folder: {:}',path)
    c = 0
    for f in big_files(path):
        c+=1
    if c == 0:
        cmd = ['rm','-rf',path]
        warn(' '.join(cmd))
        out = getout(cmd)

def manage_done_media_files(folder,destination,outfile):
    for sf in done_subfolders(folder):
        for f in big_files(sf):
            manage_file(f,destination,outfile)
        # manage_done_folder(sf)
    for f in big_done_files(folder):
        manage_file(f,destination,outfile)

if __name__=='__main__':
    args = init()
    with sys.stdout as outfile:
        manage_done_media_files(args.source,args.destination,outfile)
    # logTest()
