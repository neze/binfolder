#!/usr/bin/env python3
import subprocess,sys,yaml,mimetypes

class Tee(object):
    def __init__(self,name,mode='w'):
        self.name = name
        self.mode = mode
        self.stdout = None
        self.stream = None
    def __enter__(self):
        self.stream = open(self.name,self.mode)
        self.stdout = sys.stdout
        sys.stdout = self
    def __exit__(self,t,v,tb):
        self.stream.close()
        self.stream = None
        sys.stdout = self.stdout
        self.stdout = None
    def write(self,x):
        self.stdout.write(x)
        self.stream.write(x)
    def flush(self):
        self.stdout.flush()
        self.stream.flush()

def log(s):
    sys.stderr.write(s)
    sys.stderr.flush()

def out(s):
    sys.stdout.write(s)
    sys.stdout.flush()

f = ['find','.','-type','f','-print0']
c = 'md5sum'

_f = subprocess.Popen(f,stdout=subprocess.PIPE)
_t = subprocess.Popen(['tr','-c','-d','\\0'],stdin=_f.stdout,stdout=subprocess.PIPE)
_c = subprocess.Popen(['wc','-c'],stdin=_t.stdout,stdout=subprocess.PIPE)
_f.stdout.close()
_t.stdout.close()
_r = _c.communicate()
print('FILES TO PROCESS: %d' % int(_r[0].strip()))

_f = subprocess.Popen(f,stdout=subprocess.PIPE)
_t = subprocess.Popen(['xargs','-0','-n1024',c],stdin=_f.stdout,stdout=subprocess.PIPE)
_f.stdout.close()

db = dict()

try:
    line = _t.stdout.readline()
    while line:
        line = line.strip().decode('utf-8')
        if line:
            cs,fn = line.split(' ',1)
            cs,fn = cs.strip(),fn.strip()
            if cs in db:
                log('*')
            else:
                db[cs] = set()
                log('.')
            db[cs].add(fn)
        line = _t.stdout.readline()
except KeyboardInterrupt:
    pass
finally:
    _t.communicate()
    log('\n')

mta = dict()

for cs,fns in db.items():
    mts = set(mimetypes.guess_type(fn)[0] for fn in fns)
    if len(mts) == 1:
        mt = mts.pop()
    else:
        mt = None
    mta[mt] = mta.get(mt,0) + 1

with Tee('scan_mimetypes.log'):
    yaml.dump(mta,stream=sys.stdout,default_flow_style=False)

with Tee('scan_multiples.log'):
    for cs,fns in db.items():
        if len(fns) > 1:
            for fn in fns:
                out(cs + ':' + fn + '\n')

with Tee('scan_remove.log'):
    for cs,fns in db.items():
        if len(fns) > 1:
            for fn in list(fns)[1:]:
                out(fn + '\n')
