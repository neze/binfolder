#!/bin/bash
set -eu
# Configuration
VPN_C_NAME="${VPN_C_NAME:-yoga}"
VPN_DN="${VPN_DN:-drd.ovh}"
export SUDO_ASKPASS="$(which ssh-askpass||true)"
sudo="$(which sudo)"
[ -z "${SUDO_ASKPASS}" ] || sudo="${sudo} -A"

vpnmode_reset () {
# Get currently active connection
## Get nmcli cid
IFS=: read OLD_C_NAME OLD_C_UUID _ <<<\
  "$(nmcli --color no --terse --fields NAME,UUID\
    connection show --active | head -n1 | tr -d '\n')"
printf "Active connection: %s (%s)\n" "${OLD_C_NAME}" "${OLD_C_UUID}"
# Disable ipv6
$sudo sysctl -w\
  net.ipv6.conf.default.disable_ipv6=0\
  net.ipv6.conf.all.disable_ipv6=0
printf "IPv6 enabled\n"
$sudo systemctl restart NetworkManager.service
printf "Network restarted\n"
# Start back connection
nmcli connection up "${OLD_C_UUID}" &>/dev/null ||
nmcli connection up "${OLD_C_UUID}" &>/dev/null ||
nmcli connection up "${OLD_C_UUID}" &>/dev/null ||
! printf >&2 "Failed to start: %s\n" "${OLD_C_NAME}"
printf "Started: %s\n" "${OLD_C_NAME}"
}

vpnmode_on () {
# Get currently active connection
## Get nmcli cid
IFS=: read OLD_C_NAME OLD_C_UUID _ <<<\
  "$(nmcli --color no --terse --fields NAME,UUID\
    connection show --active | head -n1 | tr -d '\n')"
printf "Active connection: %s (%s)\n" "${OLD_C_NAME}" "${OLD_C_UUID}"
## Get my outside IP address
OLD_WMIP="$(curl -s4 ifconfig.co)"
printf "Current IP address: %s\n" "${OLD_WMIP}"

# Get vpn information
IFS=: read VPN_C_NAME VPN_C_UUID _ <<<\
  "$(nmcli --color no --terse --fields connection.id,connection.uuid\
    connection show "${VPN_C_NAME}" | cut -d: -f2- | tr '\n' ':')"
printf "VPN connection: %s (%s)\n" "${VPN_C_NAME}" "${VPN_C_UUID}"

# Disable networking
$sudo systemctl stop NetworkManager.service
$sudo systemctl stop networking.service
printf "Network disabled\n"

# Disable ipv6
$sudo sysctl -w\
  net.ipv6.conf.default.disable_ipv6=1\
  net.ipv6.conf.all.disable_ipv6=1
printf "IPv6 disabled\n"

# Re-enable networking
$sudo systemctl start networking.service
$sudo systemctl start NetworkManager.service
printf "Network enabled\n"

# Temporary disable automatic connections
nmcli --color no --terse --fields UUID connection show |
  xargs -iUUID nmcli connection modify --temporary UUID\
    connection.autoconnect no
printf "No automatic connection\n"
nmcli --color no --terse --fields UUID connection show --active |
  xargs -iUUID nmcli connection down UUID
printf "Every connection down\n"

# Temporarily set vpn by default
nmcli connection modify --temporary "${OLD_C_UUID}"\
  connection.secondaries "${VPN_C_UUID}"
printf "Dependency: %s -> %s\n" "${OLD_C_NAME}" "${VPN_C_NAME}"
nmcli connection modify --temporary "${VPN_C_UUID}"\
  connection.autoconnect yes
printf "Autoconnect: %s\n" "${VPN_C_NAME}"
nmcli connection modify --temporary "${OLD_C_UUID}"\
  connection.autoconnect yes
printf "Autoconnect: %s\n" "${OLD_C_NAME}"

# Start back connection
nmcli connection up "${OLD_C_UUID}" &>/dev/null ||
nmcli connection up "${OLD_C_UUID}" &>/dev/null ||
nmcli connection up "${OLD_C_UUID}" &>/dev/null ||
! printf >&2 "Failed to start: %s\n" "${OLD_C_NAME}"
printf "Started: %s\n" "${OLD_C_NAME}"

# Check vpn
## Check no ipv6
! curl -s6 ifconfig.co 2>/dev/null
printf "IPv6 is indeed disabled\n"
## Get my outside IP address
NEW_WMIP="$(curl -s4 ifconfig.co|sed 's/\./\\./g')"
printf "Current IP address: %s\n" "${NEW_WMIP//\\/}"
## Check coherency
host -t A "${VPN_DN}" | awk '{ print $NF }' | grep -q '^'${NEW_WMIP}'$'
printf "Current IP address coherent with VPN\n"
}

case "${1:-on}" in
  "on"|"up"|"u"|"1") vpnmode_on;;
  "off"|"down"|"d"|"0"|"reset") vpnmode_reset;;
  *) exit 1;;
esac
set +eu
