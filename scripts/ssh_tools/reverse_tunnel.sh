#!/usr/bin/env bash
# Usage: reverse_tunnel.sh <identifiant> <serveur> <port distant> <port local>
set -ue
APP_ID="$(id -nu)-tunnel-$1"
REMOTE_HOST="$2"
REMOTE_PORT="$3"
LOCAL_PORT="$4"

LOCK_FILE="/tmp/${APP_ID}.lock"

log () {
  echo >&2 "$@"
}

# Check service
if ssh "${REMOTE_HOST}" "nc -zw2000 localhost ${REMOTE_PORT}" ; then #>/dev/null 2>/dev/null; then
  log "Open port check for '${APP_ID}': OK"
else
  log "Open port check for '${APP_ID}': KO. Killing screen session."
  screen -X -S "${APP_ID}" kill 2>/dev/null || true
fi
/usr/bin/screen -dmS "${APP_ID}" /usr/bin/flock -n "${LOCK_FILE}" /usr/bin/ssh -NR 127.0.0.1:${REMOTE_PORT}:127.0.0.1:${LOCAL_PORT} "${REMOTE_HOST}"
