#!/usr/bin/env bash
set -o pipefail

vpn_check () {
  country_check=$(curl -Lsm 10 ifconfig.co/json | python3 -c 'import json,sys; print(json.loads(sys.stdin.read() or """{"country_iso":null}""")["country_iso"]);')
  if [ $? -ne 0 ]; then
    echo "net: KO"
    return 1
  fi
  echo "net: OK"
  if [ $country_check = "FR" ]; then
    echo "vpn: KO, $country_check"
    return 1
  fi
  echo "vpn: OK, $country_check"
}

dat_ensure () {
  if [ -d "/media/neze/data" ]; then
    echo "dat: OK"
  else
    if usb.py m data &>/dev/null; then
      echo "dat: mounted"
    else
      echo "dat: KO"
      return 1
    fi
  fi
}

tnt_check () {
  if systemctl --quiet status transmission-daemon.service &>/dev/null; then
    echo "tnt: OK"
    return 0
  fi
  echo "tnt: KO"
  return 1
}

tnt_start () {
  echo "tnt: starting..."
  sudo systemctl start transmission-daemon.service
}

tnt_stop () {
  echo "tnt: stopping..."
  sudo systemctl stop transmission-daemon.service
}

vpn_stop () {
  echo "vpn: stopping..."
  sudo systemctl stop openvpn@client.service
  echo "net: stopping..."
  sudo ifdown eth0 &>/dev/null || sudo ifconfig eth0 down &>/dev/null
}

vpn_start () {
  echo "net: starting..."
  sudo ifup eth0 &>/dev/null
  echo "vpn: starting..."
  sudo systemctl start openvpn@client.service
  sleep 30
}

if ! vpn_check; then
  if tnt_check; then
    tnt_stop
  fi
  vpn_stop
  vpn_start
  if ! vpn_check; then
    exit 1
  fi
fi
dat_ensure
if ! tnt_check; then
  tnt_start
fi
