#!/usr/bin/env bash
#ZERO="$(realpath "$(dirname "${BASH_SOURCE[0]-0}")")"

set -ue
cd "$(realpath "$1")"
tag -0Rm '*' | xargs -0 tag -s ''
