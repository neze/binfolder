#!/usr/bin/env bash
set -ue
while getopts "s:d:" opt; do
  case "$opt" in
    s) S_SRC="$(realpath "${OPTARG}")";;
    d) S_DST="$(realpath "${OPTARG}")";;
  esac
done
T="$(mktemp -d)"
trap "rm -rf '$T'" EXIT

pushd "$S_SRC" &>/dev/null
tag -f 'Nouveau' . | sed 's%^'"$S_SRC"'%.%' | sort > "$T/src.txt"
pushd "$S_DST" &>/dev/null
find . | sort > "$T/dst.txt"
popd &>/dev/null
comm -12 "$T/src.txt" "$T/dst.txt" | gxargs --no-run-if-empty -d'\n' tag -r 'Nouveau'
