ls *.* | sed 'p; s/\.[^.]*$/\L&/; s/\.jpeg$/.jpg/; s/IMG_//;' | xargs -d'\n' -n2 mv 2>/dev/null
mkdir -p heic
ls *.heic | sed 'p; s/\.heic$/.mov/;' | xargs -d'\n' mv -t heic 2>/dev/null
