#!/usr/bin/env bash
# share_to_disc.sh -s/Volumes/DRD202009/Videos/Series -d/Volumes/T5/Videos/Series -o/Volumes/T5/done.txt -i/Volumes/T5/wish.txt
set -ue

while getopts "s:d:o:i:" opt; do
  case "$opt" in
    s) RS_SOURCE="$OPTARG";;
    d) RS_DEST="$OPTARG";;
    o) RS_OUTPUT="$OPTARG";;
    i) RS_INPUT="$OPTARG";;
  esac
done

T="$(mktemp -d)"
trap "rm -rf '$T'" EXIT

sed 's%$%/%' < "$RS_INPUT" > "$T/wish.txt"
cat << EOF >> "$T/wish.txt"
-type
f
EOF
pushd "$RS_SOURCE" &>/dev/null

echo >&2 "Analysing available files..."
gxargs -d'\n' gfind < "$T/wish.txt" | sort > "$T/list.txt"
[ -f "$RS_OUTPUT" ] || touch "$RS_OUTPUT"
sort < "$RS_OUTPUT" > "$T/done.txt"
comm -23 "$T/list.txt" "$T/done.txt" > "$T/new.txt"

mkdir -p "$RS_DEST"
rsync -rv --size-only --progress --files-from="$T/new.txt" "$RS_SOURCE" "$RS_DEST"
cat "$T/done.txt" "$T/new.txt" | sort -u > "$RS_OUTPUT"
