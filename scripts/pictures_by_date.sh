#!/usr/bin/env bash
while [ $# -gt 0 ]; do
  arg="$1"; shift
  original="$arg"
  cdate="$(exiftool -DateTimeOriginal -d '%Y/%Y-%m/%Y-%m-%d/%Y-%m-%d_%H-%M-%S' -t "$original" | awk -F'\t' '{ print $2 }')"
  if [ -z "$cdate" ]; then
    echo >&2 "$arg"
    continue
  fi
  original_name="$(basename "$original")"
  destination="./$cdate $original_name"
  destination_folder="$(dirname "$destination")"

  mkdir -p "$destination_folder"
  ln "$original" "$destination"
done
