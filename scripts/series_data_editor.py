import os,pickle,yaml

def getDataFilename():
    return os.path.join(os.path.abspath(os.path.dirname(__file__)),'.data')

_data = None
def getData():
    global _data
    if _data is not None:
        return _data
    filename = getDataFilename()
    if os.path.exists(filename):
        with open(filename,'rb') as input:
            data = pickle.load(input)
    else:
        data = dict()
    if 'series' not in data:
        data['series'] = dict()
    putData(data)
    return data

def putData(data):
    global _data
    filename = getDataFilename()
    with open(filename,'wb') as output:
        pickle.dump(data,output,pickle.HIGHEST_PROTOCOL)
    _data = data
    return

def dumpData(filename):
    data = getData()
    with open(filename,'w') as f:
        yaml.dump(data, f, Dumper=yaml.Dumper, default_flow_style=False)

def loadData(filename):
    with open(filename,'r') as f:
        data = yaml.load(f, Loader=yaml.Loader)
    putData(data)
