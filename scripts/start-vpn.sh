echo -n "Set ipv6 to link local only"
nmcli connection modify --temporary 'm&c' ipv6.method "link-local"
for i in `seq 1 4`; do echo -n "."; sleep 1; done; echo ""
echo -n "Connect back to wifi"
nmcli connection up 'm&c' >/dev/null
for i in `seq 1 4`; do echo -n "."; sleep 1; done; echo ""
echo -n "Connect to VPN"
nmcli connection up 'neze.fr/udp' >/dev/null
for i in `seq 1 4`; do echo -n "."; sleep 1; done; echo ""
echo "IPV4: $(curl -s -4 ifconfig.co)"
echo "IPV6: $(curl -s -6 ifconfig.co)"
