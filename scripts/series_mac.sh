SERIES_DOWNLOADS="$HOME/Downloads/Torrents/Series"
SERIES_STORAGE="$HOME/Movies/Series"

# tag: brew install tag
find "$SERIES_DOWNLOADS" -type f -print | gxargs --no-run-if-empty -d'\n' tag -a 'Nouveau'

mkdir -p "$SERIES_DOWNLOADS/Subs"

# Séries/Episode/Subs/2_English.srt
find "$SERIES_DOWNLOADS" -type f '(' -regex '.*/Subs/[0-9][0-9]*_English\.srt' \
                                  -o -regex '.*/Subs/2_2\.srt' ')' |
  sort |
  gsed 'p; s%'$SERIES_DOWNLOADS'/\(.*\)/Subs/[^/]*\.srt%'$SERIES_DOWNLOADS'/Subs/\1.srt%' |
  gxargs --no-run-if-empty -d'\n' -n2 cp -vf

echo
echo "***"
echo

# Séries/Saison/Subs/Épisode/2_English.srt
find "$SERIES_DOWNLOADS" -type f '(' -regex '.*/Subs/[^/]*/[0-9][0-9]*_English\.srt' \
                                  -o -regex '.*/Subs/[^/]*/2_2\.srt' ')' |
  sort |
  gsed 'p; s%'$SERIES_DOWNLOADS'/.*/Subs/\([^/]*\)/[^/]*\.srt%'$SERIES_DOWNLOADS'/Subs/\1.srt%' |
  gxargs --no-run-if-empty -d'\n' -n2 cp -vf

series.py -C "$HOME" -s "$SERIES_DOWNLOADS" -d "$SERIES_STORAGE"

find "$SERIES_DOWNLOADS" -type f -print | gxargs --no-run-if-empty -d'\n' tag -r 'Nouveau'
