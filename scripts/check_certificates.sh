#!/bin/bash
# Standard (web) : ./check_certificates.sh google.fr:443
# STARTTLS (smtp): ./check_certificates.sh mail.indie.host:587 -starttls smtp

openssl s_client -showcerts -connect "$@" </dev/null |
sed -n '/^-----BEGIN CERTIFICATE-----$/,/^-----END CERTIFICATE-----$/ p' |
awk -F'\n' '
  BEGIN {
      showcert = "openssl x509 -noout -subject -issuer"
  }
  /-----BEGIN CERTIFICATE-----/ {
      printf "%02d: ", ind
  }
  {
      printf $0"\n" | showcert
  }
  /-----END CERTIFICATE-----/ {
      close(showcert)
      ind ++
  }
'
