#!/usr/bin/env python3
# Get data : lsblk -OaJ (json output)
# Mount/Unmount/Power-off : udisksctl
import subprocess
import json
import yaml
import sys
import argparse
import pathlib2
# from ipdb import set_trace

DEV=pathlib2.Path("/dev")

def lsblk():
    data = subprocess.check_output(['lsblk','-O','-J'],encoding='utf-8')
    return json.loads(data)

class BlockDevice(object):
    def __init__(self,data,parent=None):
        self.parent = parent
        self.update(data)
    def update(self,data):
        self.dict = data
        self.type = data["type"]
        self.kname = data["kname"]
        self.path = DEV / self.kname
        self.name = data["name"]
        self.mountpoint = data["mountpoint"]
        self.children = dict()
        for c in data.get("children",[]):
            child = blockdevice(c,parent=self)
            self.children[child.name] = child
    def __str__(self):
        return '%s(%s/%s)' % (type(self).__name__,self.kname,self.name)
    def json(self):
        key = str(self)
        val = []
        for child in self.children.values():
            val.append(child.json())
        val = val or self.mountpoint
        return { key: val } if val else key
    def match(self,kw):
        for c in self.children.values():
            m = c.match(kw)
            if m: return m
        if self.kname == kw: return self
        if kw.lower() in self.name.lower(): return self

class Disk(BlockDevice):
    def update(self,data):
        super().update(data)
        self.name = ' '.join(x.strip() for x in [data["vendor"] or "",data["model"] or "",data["serial"] or ""])
    def mount(self,key=None,options=None):
        for c in self.children.values():
            c.mount(key=key,options=options)
    def unmount(self):
        for c in self.children.values():
            c.unmount()
    def eject(self):
        self.unmount()
        subprocess.run(['udisksctl','power-off','-b',self.path]).check_returncode()

class Part(BlockDevice):
    def update(self,data):
        super().update(data)
        self.name = data["label"] or data["partlabel"] or self.name
        self.fstype = data["fstype"] or "none"
        if self.fstype.lower().startswith('crypto'):
            self.__m_cmd='unlock'
            self.__u_cmd='lock'
            self.__k_arg='--key-file'
        else:
            self.__m_cmd='mount'
            self.__u_cmd='unmount'
            self.__k_arg=None
    def mount(self,key=None,options=None):
        if len(self.children) == 0 and self.mountpoint is None:
            kwargs = dict()
            cmd = ['udisksctl',self.__m_cmd,'-b',self.path]
            if options is not None:
                cmd += ['--options',options]
            if key is not None and self.__k_arg is not None:
                cmd += [self.__k_arg,'/dev/stdin']
                kwargs["input"] = key.read()
            subprocess.run(cmd,**kwargs).check_returncode()
        get_blockdevices(1)
        for c in self.children.values():
            c.mount()
    def unmount(self):
        while len(self.children) > 0 or self.mountpoint is not None:
            subprocess.run(['udisksctl',self.__u_cmd,'-b',self.path]).check_returncode()
            get_blockdevices(1)
    def eject(self):
        self.unmount()
        if self.type == 'crypt':
            self.parent.unmount()

type2class = {
        "part": Part,
        "crypt": Part,
        "disk": Disk,
        "loop": BlockDevice,
    }

CACHE = dict()
def blockdevice(data,parent=None):
    kname=data["kname"]
    if kname not in CACHE:
        CACHE[kname] = type2class[data["type"]](data,parent=parent)
    else:
        CACHE[kname].update(data)
    return CACHE[kname]

DEVS = None
def get_blockdevices(force=0):
    global DEVS
    if DEVS is not None and not force: return
    DEVS = list()
    for dev in lsblk()["blockdevices"]:
        DEVS.append(blockdevice(dev))

ACTIONS = dict()
def register(f):
    ACTIONS[f.__name__] = f
    return f

@register
def list_devices(args):
    get_blockdevices()
    yaml.dump([b.json() for b in DEVS],stream=sys.stdout,default_flow_style=False)

# @register
# def inspect_device(args):
    # dev = args.dev
    # set_trace()

@register
def mount_device(args):
    for dev in args.dev: dev.mount(key=args.key,options=args.options)

@register
def unmount_device(args):
    for dev in args.dev: dev.unmount()

@register
def remount_device(args):
    unmount_device(args)
    mount_device(args)

@register
def eject_device(args):
    for dev in args.dev: dev.eject()

def AvailableAction(s):
    s = str(s)
    if not s:
        raise argparse.ArgumentTypeError('Empty command')
    m = list()
    for k,v in ACTIONS.items():
        if k.startswith(s):
            m.append(v)
    if len(m) == 0:
        raise argparse.ArgumentTypeError('Unknown command: "%s"' % s)
    if len(m) > 1:
        raise argparse.ArgumentTypeError('Multiple matches for command: "%s"' % s)
    return m[0]

def AvailableDevice(s):
    get_blockdevices()
    s = str(s)
    if not s:
        raise argparse.ArgumentTypeError('Empty device description')
    for dev in DEVS:
        m = dev.match(s)
        if m: return m
    raise argparse.ArgumentTypeError('No matching device: "%s"' % s)

if __name__=='__main__':
    ap = argparse.ArgumentParser(prog='usb.py')
    ap.add_argument('cmd',nargs='?',type=AvailableAction,default='list',help=','.join(x.split('_',1)[0] for x in ACTIONS.keys()))
    ap.add_argument('dev',nargs='*',type=AvailableDevice,help='device description (kname, part of name, etc.)')
    ap.add_argument('-k','--key',type=argparse.FileType(mode='rb'),default=None)
    ap.add_argument('-o','--options',type=str,default=None)
    args = ap.parse_args()
    args.cmd(args)
