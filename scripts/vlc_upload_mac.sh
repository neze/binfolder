#!/usr/bin/env bash
DRY_RUN=false
SEE_ALL=false
VERB=false
while getopts "nafv" opt; do
  case "$opt" in
    n) DRY_RUN=true;;
    a) SEE_ALL=true;;
    f) DRY_RUN=false;;
    v) VERB=true;;
  esac
done
shift $((OPTIND-1))

if [ "$#" -gt 0 ]; then
  vlc_upload.py -d undercover.local "$@" |
  gxargs -d'\n' --no-run-if-empty -n1 tag -a 'Envoyé'
  exit $?
fi

SERIES="$HOME/Movies/Series"
cd "$SERIES"

T="$(mktemp -d)"
trap "rm -rf '$T'" EXIT
if $VERB; then echo >&2 "$T"; fi

tag -Rpf 'Ignoré' > "$T/iu.txt"
tag -Rpf 'Envoyé' >> "$T/iu.txt"
tag -Rpf 'Vu' >> "$T/iu.txt"
if ! ( $DRY_RUN && $SEE_ALL ); then
  tag -Rpf 'Available' >> "$T/iu.txt"
fi
sort -u < "$T/iu.txt" > "$T/i.txt" && rm "$T/iu.txt"

grep '/$' "$T/i.txt" | sed 's%/$%%' > "$T/idu.txt"
grep -v '/$' "$T/i.txt" > "$T/ifu.txt"
gxargs --no-run-if-empty -I '{}' find '{}' -type f < "$T/idu.txt" >> "$T/ifu.txt"
sort -u < "$T/ifu.txt" > "$T/if.txt" && rm "$T/ifu.txt"

gfind "$SERIES" -type f -not -name '.DS_Store' | sort -u > "$T/f.txt"

comm -23 "$T/f.txt" "$T/if.txt" | sed 's%^'"$SERIES"'/%%' |
  {
    if $DRY_RUN; then
      gxargs -d'\n' --no-run-if-empty du -hc
    else
      gxargs -d'\n' --no-run-if-empty vlc_upload.py -d undercover.local |
      gxargs -d'\n' --no-run-if-empty -n1 tag -a 'Envoyé'
    fi
  }
