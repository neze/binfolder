#!/usr/bin/env bash
# brew tap homebrew-ffmpeg/ffmpeg
# brew install homebrew-ffmpeg/ffmpeg/ffmpeg --with-fdk-aac
find . -name '*.flac' -exec sh -c 'ffmpeg -i "$1" -map a:0 -c:a libfdk_aac -b:a 320k "${1%.flac}.m4a"' _ {} \;
