#!/usr/bin/env python3
import sys,subprocess,argparse

buffer_size = 1024

def chop_stdin(size):
    while 1:
        chunk = sys.stdin.read(size)
        if len(chunk):
            yield chunk
        else:
            return

if __name__=='__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('dotsize',type=int,nargs='?',default=6)
    args = ap.parse_args()
    for chunk in chop_stdin(buffer_size):
        image = subprocess.check_output(['qrencode','-o','-','-s','%d' % args.dotsize],input=chunk.encode('ascii'))
        subprocess.run(['display'],input=image)
