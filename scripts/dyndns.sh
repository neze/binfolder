#!/bin/bash
set -eu
HOST="${1}"
IP="${SSH_CLIENT%% *}"
DYNDNS_PATH="${DYNDNS_PATH:-${HOME}/dyndns}"
echo "${IP}" > "${DYNDNS_PATH}/${HOST}.txt"
echo "Thanks, ${HOST} (${IP})."
set +eu
