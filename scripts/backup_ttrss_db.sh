#!/bin/bash
set -ueo pipefail
BACKUP_FOLDER="/media/neze/cryptoHDDneze/Drive/Backup"
TTRSS_SRV=drd.ovh
TTRSS_DB=rss_db_1

test -d ${BACKUP_FOLDER}

case "${1:-backup}" in
  "backup")
    BACKUP_FILE="ttrssdb_$(date +%Y-%m-%d).sql.gz";
    ssh $TTRSS_SRV "docker exec -t $TTRSS_DB pg_dumpall -c -U postgres | gzip -c" | pv > "${BACKUP_FOLDER}/${BACKUP_FILE}";;
  "restore")
    MOST_RECENT_BACKUP="$(ls -1 ${BACKUP_FOLDER} | grep '\.gz$' | tail -n1)";
    cat "${BACKUP_FOLDER}/${MOST_RECENT_BACKUP}" | pv | ssh vps "gzip -d | docker exec -i ttrssdb psql -U postgres";;
  *) exit 2;;
esac
