#!/usr/bin/env bash
set -ue
while getopts "l:r:" opt; do
  case "$opt" in
    l) L_DIR="$(realpath "$OPTARG")";;
    r) R_DIR="$(realpath "$OPTARG")";;
  esac
done

xargs="$(which gxargs) --no-run-if-empty"
alias sed="gsed"

wcl () {
  wc -l | awk '{ print $1 }'
}
log () {
  echo >&2 "$(printf "$@")"
}

T="$(mktemp -d)"
trap "rm -rf '$T'" EXIT

# List folders on local and remote
ld="$T/local_dirs"
find "$L_DIR" -type d -mindepth 1 -print | sort | sed 's%^'"$L_DIR"'%%;s%^/%%;' > "$ld"
log "%d directories on local" "$(wcl < "$ld")"
rd="$T/remote_dirs"
find "$R_DIR" -type d -mindepth 1 -print | sort | sed 's%^'"$R_DIR"'%%;s%^/%%;' > "$rd"
log "%d directories on remote" "$(wcl < "$rd")"

# Folders not on local, present on remote :
# > mkdir
# > copy tags
rld="${rd}_to_local"
comm -13 "$ld" "$rd" > "$rld"
rldt="${rld}_tags"
cd "$R_DIR"
${xargs} -d'\n' tag -l < "$rld" | awk -F '\t' '{ if ( $2 ) { gsub(/[ \t]+$/,"",$1); print $2 "\n" $1; } }' > "$rldt"
cd "$L_DIR"
n="$(wcl < "$rld")"
log "%d missing folders on local" "$n"
if [ "$n" -gt 0 ]; then
  ${xargs} -d'\n' mkdir < "$rld"
fi
n="$(wcl < "$rldt")"
if [ "$n" -gt 0 ]; then
  ${xargs} -d'\n' -n2 tag -s < "$rldt"
fi

# Folders not on remote, present on local :
# > mkdir
# > copy tags
lrd="${ld}_to_remote"
comm -23 "$ld" "$rd" > "$lrd"
lrdt="${lrd}_tags"
cd "$L_DIR"
${xargs} -d'\n' tag -l < "$lrd" | awk -F '\t' '{ if ( $2 ) { gsub(/[ \t]+$/,"",$1); print $2 "\n" $1; } }' > "$lrdt"
cd "$R_DIR"
n="$(wcl < "$lrd")"
log "%d missing folders on remote" "$n"
if [ "$n" -gt 0 ]; then
  ${xargs} -d'\n' mkdir < "$lrd"
fi
n="$(wcl < "$lrdt")"
if [ "$n" -gt 0 ]; then
  ${xargs} -d'\n' -n2 tag -s < "$lrdt"
fi

# List files on local and remote
lf="$T/local_files"
find "$L_DIR" -type f -mindepth 1 -print | sort | sed 's%^'"$L_DIR"'%%;s%^/%%;' > "$lf"
log "%d files on local" "$(wcl < "$lf")"
rf="$T/remote_files"
find "$R_DIR" -type f -mindepth 1 -print | sort | sed 's%^'"$R_DIR"'%%;s%^/%%;' > "$rf"
log "%d files on remote" "$(wcl < "$rf")"

# Not on local, present on remote :
# > touch
# > copy tags
rlf="${rf}_to_local"
comm -13 "$lf" "$rf" > "$rlf"
rlft="${rlf}_tags"
cd "$R_DIR"
${xargs} -d'\n' tag -l < "$rlf" | awk -F '\t' '{ if ( $2 ) { gsub(/[ \t]+$/,"",$1); print $2 "\n" $1; } }' > "$rlft"
cd "$L_DIR"
n="$(wcl < "$rlf")"
log "%d missing files on local : marking as Available" "$n"
if [ "$n" -gt 0 ]; then
  ${xargs} -d'\n' touch < "$rlf"
fi
o="$(wcl < "$rlft")"
if [ "$o" -gt 0 ]; then
  ${xargs} -d'\n' -n2 tag -s < "$rlft"
fi
if [ "$n" -gt 0 ]; then
  ${xargs} -d'\n' tag -a 'Available' < "$rlf"
fi

# Not on remote, present on local :
# > rsync
# > remove tag 'Nouveau'
lrf="${lf}_to_remote"
comm -23 "$lf" "$rf" > "$lrf"
n="$(wcl < "$lrf")"
log "%d missing files on remote : copying and unmarking" "$n"
if [ "$n" -gt 0 ]; then
  rsync --progress -aE --files-from="$lrf" "$L_DIR" "$R_DIR"
  cd "$L_DIR"
  ${xargs} -d'\n' tag -r 'Nouveau' < "$lrf"
fi

# Tagged 'Remove'
# > remove files from local
# > remove files from remote
rmf="$T/files_to_remove"
tag -Rm 'Remove' "$L_DIR" | sort -r > "$rmf"
rmn="$(wcl < "$rmf")"
log "%d files to remove on local and remote : asking first" "$rmn"
if [ "$rmn" -gt 0 ]; then
  rma="$(osascript -e 'button returned of (display dialog "Remove '"$rmn"' item(s) from local and remote folders?" with title "Synchronisation" with icon caution buttons { "No", "Yes" } default button "Yes")')"
  if [ "x$rma" = "xYes" ]; then
    cd "$L_DIR"
    ${xargs} -d'\n' rm -d < "$rmf"
    cd "$R_DIR"
    ${xargs} -d'\n' rm -d < "$rmf"
  fi
fi

# Tagged 'Pull'
# > remember tags on local files
# > remove local files
# > rsync remote to local
# > set tags on local
# > remove tags 'Available' and 'Pull'
plf="$T/files_to_pull"
tag -Rm 'Pull' "$L_DIR" > "$plf"
plft="${plf}_tags"
cd "$L_DIR"
${xargs} -d'\n' tag -l < "$plf" | awk -F '\t' '{ if ( $2 ) { gsub(/[ \t]+$/,"",$1); print $2 "\n" $1; } }' > "$plft"
n="$(wcl < "$plf")"
log "%d files to pull from remote" "$n"
#${xargs} -d'\n' rm < "$plf"
if [ "$n" -gt 0 ]; then
  rsync --progress -aE --files-from="$plf" "$R_DIR" "$L_DIR"
fi
o="$(wcl < "$plft")"
if [ "$o" -gt 0 ]; then
  ${xargs} -d'\n' -n2 tag -s < "$plft"
fi
if [ "$n" -gt 0 ]; then
  ${xargs} -d'\n' tag -r 'Available' < "$plf"
  ${xargs} -d'\n' tag -r 'Pull' < "$plf"
fi

# Different tags on local and remote
# > get tags from local
# > set tags on remote
lt="$T/local_tags"
tag -Rtm '*' "$L_DIR" | awk -F '\t' '{ gsub(/[ \t]+$/,"",$1); print $1 "\t" $2; }' | sort > "$lt"
log "%d tagged files on local" "$(wcl < "$lt")"
rt="$T/remote_tags"
tag -Rtm '*' "$R_DIR" | awk -F '\t' '{ gsub(/[ \t]+$/,"",$1); print $1 "\t" $2; }' | sort > "$rt"
log "%d tagged files on remote" "$(wcl < "$rt")"
ltr="${lt}_to_remote"
comm -23 "$lt" "$rt" > "$ltr"
n="$(wcl < "$ltr")"
log "%d files to tag on remote" "$n"
if [ "$n" -gt 0 ]; then
  cd "$R_DIR"
  awk -F '\t' '{ if ( $2 ) { gsub(/[ \t]+$/,"",$1); print $2 "\n" $1; } }' < "$ltr" |
  ${xargs} -d'\n' -n2 tag -s
fi
lnt="$T/local_not_tagged"
tag -Rm '' "$L_DIR" | sort > "$lnt"
log "%d untagged files on local" "$(wcl < "$lnt")"
tag -Rm '*' "$R_DIR" | sort > "$rt"
ltr="${lnt}_to_remote"
comm -12 "$lnt" "$rt" > "$ltr"
n="$(wcl < "$ltr")"
log "%d files to untag on remote" "$n"
if [ "$n" -gt 0 ]; then
  cd "$R_DIR"
  ${xargs} -d'\n' tag -s '' < "$ltr"
fi
set +ue
