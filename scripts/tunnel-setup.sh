#!/usr/bin/env bash
set -ue
v4rx="(([1]?[0-9]{1,2})|(2(([0-4][0-9])|(5[0-5]))))"
v4rx="$v4rx\\.$v4rx\\.$v4rx\\.$v4rx"
prx="(([1-5]?[0-9]{1,4})|(6(([0-4][0-9]{3})|(5(([0-4][0-9]{2})|(5(([0-2][0-9])|(3[0-5]))))))))"

usage () {
  echo >&2 "usage: tunnel-setup.sh <remote-addr> <remote-port>"
  if [ $# -gt 0 ]; then
    echo >&2 "error:" "$@"
  fi
  exit 1
}

if [ $# -ne 2 ]; then usage; fi
REMOTE_ADDR="$1"
if ! echo "${REMOTE_ADDR}" | egrep -qx "${v4rx}"; then usage "Invalid IPv4 address."; fi
REMOTE_PORT="$2"
if ! echo "${REMOTE_PORT}" | egrep -qx "${prx}"; then usage "Invalid port."; fi

TEMPDIR="$(mktemp -d)"

RANDOM_USER="tunnel-$(openssl rand -hex 4)"
RANDOM_PORT="$(python3 -c "import random; print(random.randrange(1<<10,1<<16))")"

SERVER_ADDR="neze.fr"
SERVER_USER="tunnel"

LOCAL_PORT="$(python3 -c "import random; print(random.randrange(1<<10,1<<16))")"

echo >&2 "Generating ssh-key..."
ssh-keygen >/dev/null -t rsa -b 2048 -C "${RANDOM_USER}" -N "" -f "${TEMPDIR}/id_rsa"

echo >&2 "Authorizing ssh-key on ${SERVER_USER}@${SERVER_ADDR}..."
ssh >/dev/null -oUser=${SERVER_USER} ${SERVER_ADDR} tee -a .ssh/authorized_keys < "${TEMPDIR}/id_rsa.pub"

echo >&2 "Generating script remote.sh..."
{
cat <<EOT
#!/usr/bin/env bash
TEMPDIR="\$(mktemp -d)"
cd "\${TEMPDIR}"
cat <<EOF >id_rsa
EOT
cat "${TEMPDIR}/id_rsa"
cat <<EOT
EOF
chmod 600 id_rsa
ssh -oIdentityFile=id_rsa -oStrictHostKeyChecking=accept-new -oUserKnownHostsFile=/dev/null -oUser=${SERVER_USER} -NR 127.0.0.1:${RANDOM_PORT}:${REMOTE_ADDR}:${REMOTE_PORT} ${SERVER_ADDR}
echo cleanup
rm id_rsa
cd - >/dev/null
rmdir "\${TEMPDIR}"
EOT
} > "${TEMPDIR}/remote.sh"

echo >&2 "Send the remote.sh file to the remote machine and have them run it, then exit the terminal below."
pushd "${TEMPDIR}" >/dev/null
bash -l
popd >/dev/null

{ sleep 1; for i in `seq 1 9`; do echo -n >&2 "."; sleep 1; done; echo >&2 "Starting browser..."; xdg-open "http://127.0.0.1:${LOCAL_PORT}/" >/dev/null; } &

echo >&2 "Setting local tunnel..."
ssh -oUser=${SERVER_USER} -NL 127.0.0.1:${LOCAL_PORT}:127.0.0.1:${RANDOM_PORT} ${SERVER_ADDR}

echo >&2 "Removing authorization on ${SERVER_USER}@${SERVER_ADDR}..."
ssh -oUser=${SERVER_USER} ${SERVER_ADDR} "sed -i '/${RANDOM_USER}$/ d' .ssh/authorized_keys"

rm -rf "${TEMPDIR}"
set +ue
